import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import axios from 'axios';
import Header from './Header.js';
import Sidebar from './Sidebar';
import HotelList from './HotelList';
/*import Footer from './Footer';*/

class App extends Component {
  constructor(props){
      super(props);
      this.state = {page: 1, sortBy: "", filters: {}};
      this.doSort = this.doSort.bind(this);
      this.onFilterChanged = this.onFilterChanged.bind(this);

  }

 nextPage(){
    var currentPage = this.state.page;
    console.log(currentPage);
    currentPage = currentPage +1;
     this.setState(()=> ({page: currentPage}));
     this.doSort = this.doSort.bind(this)
 }

 prevPage(){
     var currentPage = this.state.page;
     if(currentPage === 1){
         return;
     }
     if(currentPage > 1){
         currentPage = currentPage -1;
         this.setState(()=>({page: currentPage}))
     }
 }

 doSort(){
      this.setState(()=> ({sortBy: 'reviews.rating'}));
 }

    onFilterChanged(newFilters){
        console.log('onFilterChanged of App.js called with', newFilters);
        this.setState(()=> ({filters: newFilters}));
    }

  render() {
     console.log('calling render', this.state);
    return (
      <div className="App"  ref='main'>
        <Header name="surbhi soni"/>
          <div className="container-fluid">
              <div className="row">
                <Sidebar  onFilterChanged={this.onFilterChanged}/>
                  <div className="clearfix"></div>
                  <div className="clearfix"></div>
                  <main className="col-md-9 offset-sm-3 col-md-10 offset-md-2 pt-3" id='hotel-list'>
                      <a href="#" onClick={this.doSort}>Sort By</a>
                      <HotelList page={this.state.page} sortBy={this.state.sortBy} filters={this.state.filters}/>
                  </main>
                  <footer className="footer">
                      <nav aria-label="Page navigation example" className="footer-container">
                          <ul className="pagination justify-content-center">
                              <li className="page-item">
                                  <a className="page-link" tabIndex="-1" onClick={this.prevPage.bind(this)}>Previous</a>
                              </li>
                              <li className="page-item"><a className="page-link" onClick={this.nextPage.bind(this)}>1</a></li>
                              <li className="page-item"><a className="page-link" onClick={this.nextPage.bind(this)}>2</a></li>
                              <li className="page-item"><a className="page-link" onClick={this.nextPage.bind(this)}>3</a></li>
                              <li className="page-item"><a className="page-link" onClick={this.nextPage.bind(this)}>4</a></li>
                              <li className="page-item"><a className="page-link" onClick={this.nextPage.bind(this)}>5</a></li>
                              <li className="page-item"><a className="page-link" onClick={this.nextPage.bind(this)}>6</a></li>
                              <li className="page-item">
                                  <a className="page-link" onClick={this.nextPage.bind(this)}>Next</a>
                              </li>
                          </ul>
                      </nav>
                  </footer>
              </div>
          </div>
      </div>
    );
  }
}

export default App;