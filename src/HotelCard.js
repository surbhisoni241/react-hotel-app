import React from 'react';

class HotelCard extends React.Component{
    constructor(){
        super()
    }

    render(){
        let hotel = this.props.hotel;
        return(
            <div key={hotel}>
                <div className="col-lg-11 placeholder hotel-wrapper">
                    <img className="col-lg-3" src="assets/img/hotel.jpg" />
                        <div className="col-lg-8">
                            <div className="hotel-heading">
                                <h3 className="hotel-name font-color">{hotel.name}</h3>
                                <ul className="float-right">
                                    <li className="ratings ">Ratings: {hotel.reviews.rating}</li>
                                </ul>
                            </div>
                            <div className="hotel-fluid">
                                <label className="catergories">{hotel.categories}</label><br/>
                                <span className="hotel-address"><b>Address:</b> {hotel.address}</span> <a> map </a>
                                <p className="city-country">
                                    <b>City:</b> {hotel.city} - <b>Country:</b> {hotel.country} </p>
                            </div>
                        </div>
                </div>
            </div>
        );
    }
}

export default HotelCard;