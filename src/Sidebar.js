import React from 'react'

class Sidebar extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            filters: {
                city: null,
            }
        };
        this.selectCity = this.selectCity.bind(this);
    }

    selectCity(e){
        var selectedCity = e.target.value;
        var oldFilters = this.state.filters;
        oldFilters.city = selectedCity;
        this.setState(()=> ({oldFilters:oldFilters}));
        this.props.onFilterChanged(oldFilters);
    }

    render(){
        return(
            <div>
                <nav className="col-sm-3 col-md-2 hidden-xs-down bg-nav sidebar">
                    <ul className="nav nav-pills flex-column">
                        <li className="nav-item">
                            <a className="nav-link board font-color" href="#">Hotels <span
                                className="sr-only"></span></a>
                        </li>
                        <li className="nav-item board">
                            <a className="nav-link font-color" href="#">Filters <span className="sr-only"></span></a>
                            <ul className="filter">
                                <li>
                                       <select className="form-control form-control-sm"  onChange={this.selectCity} ref="city" >
                                            <option>City</option>
                                            <option>Percival</option>
                                            <option>Mableton</option>
                                            <option>Los Olivos</option>
                                            <option>Palm Springs</option>
                                       </select>
                                </li>

                                <li>
                                        <select className="form-control form-control-sm">
                                            <option>Category</option>
                                            <option>Hotels</option>
                                            <option>Motels</option>
                                            <option>Lodging</option>
                                            <option>Restaurants</option>
                                        </select>
                                </li>
                                <li>
                                        <select className="form-control form-control-sm">
                                            <option>Ratings</option>
                                            <option> > 5.0 </option>
                                            <option> 5.0 </option>
                                            <option> 4.0 </option>
                                            <option> 3.0 </option>
                                            <option> 2.0 </option>
                                            <option> 1.0 </option>
                                            <option> 0.0 </option>
                                        </select>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        );
    }
}

export default Sidebar;