import React from 'react';
import HotelCard from './HotelCard';
import axios from 'axios';

class HotelList extends React.Component{
    constructor(props){
        super(props)
        this.state = {hotels: []}
    }

    fetchData(page, sortBy, filters){
        let uri = `http://localhost:4020/hotels?page=${page}`;
        console.log("sortBy", sortBy);
        if (filters)
            Object.keys(filters).forEach((key)=> {
                uri += '&' + key + '=' + filters[key];
            });
        if (sortBy){
            uri += '&sortBy=' + sortBy;
        }
        axios.get(uri)
            .then(response => {
                console.log('fetch data called');
                console.log("hotels", response.data);
                this.setState(() => ({hotels: response.data.hotels, count: response.data.count}))
            })
            .catch(err => {
                console.log('err', err);
            });
    }
    componentWillReceiveProps(nextProps){
        console.log('nextProps', nextProps);
        this.fetchData(nextProps.page, nextProps.sortBy, nextProps.filters);
    }
    componentDidMount() {
        console.log('HotelList componentDidMount Called', this.props.page);
        this.fetchData(1);

    }
    render(){
        console.log('HotelList render called');
        if (!this.state.hotels)
        {
            return (<p>No Hotel Found</p>);
        }
        return (<div>
                <div className="row head-wrap"><h3 className="font-color hotels-length"> {this.state.count} - Hotels Available.</h3></div>
                {this.state.hotels.map((hotel)=> (
                    <HotelCard key={hotel._id} hotel={hotel}/>
                ))}
            </div>
        );
     }
}

export default HotelList;